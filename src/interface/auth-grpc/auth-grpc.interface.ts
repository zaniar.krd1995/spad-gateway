import { Observable } from "rxjs";
import { ISigninDTO } from "./signin.interface";
import { IRegisterDTO } from "./register.interface";
import { ISignupDTO } from "./signup.interface";

export interface IAuthGrpcService {
  register(dto: IRegisterDTO): Observable<any>;
  signup(dto: ISignupDTO): Observable<any>;
  signin(dto: ISigninDTO): Observable<any>;
}