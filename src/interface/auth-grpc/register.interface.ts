import { ISigninDTO } from "./signin.interface";

export interface IRegisterDTO extends ISigninDTO {
  passwordConfirm: string;
}