import { Observable } from "rxjs";

export interface IHotelGrpcService {
  searchHotelByName(name: ISearchHotelParams): Observable<string[]>;
}

export interface ISearchHotelParams {
  name: string;
  city: string;
}