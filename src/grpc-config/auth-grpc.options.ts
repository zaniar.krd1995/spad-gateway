import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const authGrpcOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'localhost:50052',
    package: 'auth',
    protoPath: join(__dirname, '../../src/proto/auth.proto'),
  },
}