import { ClientOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

export const hotelGrpcOptions: ClientOptions = {
  transport: Transport.GRPC,
  options: {
    url: 'localhost:50053',
    package: 'hotel',
    protoPath: join(__dirname, '../../src/proto/hotel.proto'),
  },
}