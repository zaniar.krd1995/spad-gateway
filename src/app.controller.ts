import { Controller, Post, Req } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { IAuthGrpcService } from './interface/auth-grpc/auth-grpc.interface';
import { authGrpcOptions } from './grpc-config/auth-grpc.options';
import { hotelGrpcOptions } from './grpc-config/hotel-grpc.options';
import { IHotelGrpcService } from './interface/hotel-grpc/hotel-grpc.interface';
import { firstValueFrom } from 'rxjs';
import { Request } from 'express';

@Controller()
export class AppController {
  
  // auth

  @Client(authGrpcOptions)
  private authGrpcClient: ClientGrpc;

  private authGrpcService: IAuthGrpcService;


  // hotel

  @Client(hotelGrpcOptions)
  private hotelGrpcClient: ClientGrpc;

  private hotelGrpcService: IHotelGrpcService;


  onModuleInit() {                                                            
    this.authGrpcService = this.authGrpcClient.getService<IAuthGrpcService>('AuthService');
    this.hotelGrpcService = this.hotelGrpcClient.getService<IHotelGrpcService>('HotelService');
  }         
  
  
  @Post('register')
  async register(@Req() request: Request) {
    const result = await firstValueFrom(this.authGrpcService.register({
      email: request.body?.email,
      password: request.body?.password,
      passwordConfirm: request.body?.passwordConfirm
    }));
    
    return result;
  }


  @Post('signup')
  async signup(@Req() request: Request) {
    const result = await firstValueFrom(this.authGrpcService.signup({
      email: request.body?.email,
      otp: request.body?.otp
    }));
    
    return result;
  }
  
  
  @Post('signin')
  async signin(@Req() request: Request) {
    const result = await firstValueFrom(this.authGrpcService.signin({
      email: request.body?.email,
      password: request.body?.password,
    }));

    return result;
  }

}
