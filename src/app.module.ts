import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AppController } from './app.controller';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
  ],
  controllers: [AppController],
  providers: [JwtStrategy],
})
export class AppModule {}
